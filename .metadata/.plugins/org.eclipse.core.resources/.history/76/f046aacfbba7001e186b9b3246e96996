package org.nymble;

import java.util.ArrayList;
import java.util.List;

public class Passenger {

	 private String name;
	    private int passengerNumber;
	    private PassengerType type;
	    private double balance; // Applicable for Standard and Gold passengers

		private List<Activity> signedActivities;
	    public double getBalance() {
			return balance;
		}

		public void setBalance(double balance) {
			this.balance = balance;
		}


	    public Passenger(String name, int passengerNumber) {
	        this.name = name;
	        this.passengerNumber = passengerNumber;
	        this.signedActivities = new ArrayList<>();
	    }

	    public String getName() {
	        return name;
	    }

	    public int getPassengerNumber() {
	        return passengerNumber;
	    }

	    public List<Activity> getSignedActivities() {
	        return signedActivities;
	    }

	    public void signUpForActivity(Activity activity) {
	        signedActivities.add(activity);
	    }
	   
	    public boolean cancelActivity(Activity activity) {
	        return signedActivities.remove(activity);
	    }

	    // Other methods for managing signed activities

	    @Override
	    public String toString() {
	        return "Passenger{" +
	                "name='" + name + '\'' +
	                ", passengerNumber=" + passengerNumber +
	                ", signedActivities=" + signedActivities +
	                '}';
	    }
	}

	class StandardPassenger extends Passenger {
	    private double balance;

	    public StandardPassenger(String name, int passengerNumber, double balance) {
	        super(name, passengerNumber);
	        this.balance = balance;
	    }

	    public double getBalance() {
	        return balance;
	    }

	    public void deductBalance(double amount) {
	        if (balance >= amount) {
	            balance -= amount;
	        } else {
	            System.out.println("Insufficient balance");
	        }
	    }

	    // Other methods specific to StandardPassenger
	}

	class GoldPassenger extends Passenger {
	    private double balance;

	    public GoldPassenger(String name, int passengerNumber, double balance) {
	        super(name, passengerNumber);
	        this.balance = balance;
	    }

	    public double getBalance() {
	        return balance;
	    }

	    public void applyDiscount(double amount) {
	        if (balance >= amount) {
	            balance -= amount;
	        } else {
	            System.out.println("Insufficient balance");
	        }
	    }

	    // Other methods specific to GoldPassenger
	}

	class PremiumPassenger extends Passenger {
	    public PremiumPassenger(String name, int passengerNumber) {
	        super(name, passengerNumber);
	    }

	    // Methods specific to PremiumPassenger
	}
	