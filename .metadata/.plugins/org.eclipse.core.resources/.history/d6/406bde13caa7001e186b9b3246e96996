package org.nymble;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class DestinationTest {

	@Test
    public void testAddActivity() {
        Destination destination = new Destination("Beach");
        Activity activity = new Activity("Surfing", "Water sport", 25.0, 15);

        destination.addActivity(activity);

        List<Activity> activities = destination.getActivities();
        assertTrue(activities.contains(activity)); // Check if activity is added to destination's activities
    }

    @Test
    public void testRemoveActivity() {
        Destination destination = new Destination("Mountain");
        Activity activity = new Activity("Hiking", "Outdoor activity", 0, 20);

        destination.addActivity(activity);
        assertTrue(destination.hasActivity(activity)); // Confirm activity is present

        destination.removeActivity(activity);
        assertFalse(destination.hasActivity(activity)); // Ensure activity is removed
    }

    @Test
    public void testIsActivityAvailable() {
        Destination destination = new Destination("Lake");
        Activity activity = new Activity("Boating", "Leisure activity", 50.0, 10);

        destination.addActivity(activity);
        assertTrue(destination.isActivityAvailable(activity)); // Check if activity is available initially

        Activity fullyBookedActivity = new Activity("Fishing", "Relaxing sport", 0, 5);
        destination.addActivity(fullyBookedActivity);

        assertFalse(destination.isActivityAvailable(fullyBookedActivity)); // Ensure fully booked activity is unavailable
    }
}
