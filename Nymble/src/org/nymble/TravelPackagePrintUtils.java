package org.nymble;

public class TravelPackagePrintUtils implements TravelPackagePrinting {
	
	
       public TravelPackagePrintUtils() {
		// TODO Auto-generated constructor stub
	}
     
	@Override
	public  void printItinerary(TravelPackage travelPackage) {
		System.out.println("Travel Package: " + travelPackage.getName() + " Itinerary:");
		for (Destination destination : travelPackage.getItinerary()) {
			System.out.println("Destination: " + destination.getName());
			System.out.println("Activities:");
			for (Activity activity : destination.getActivities()) {
				System.out.println("- Name: " + activity.getName() + ", Cost: " + activity.getCost() + ", Capacity: "
						+ activity.getCapacity() + ", Description: " + activity.getDescription());
			}
		}
	}

	@Override
	public void printPassengerList(TravelPackage travelPackage) {
		System.out.println("Passenger List for Travel Package\n Travel package name : " + travelPackage.getName());
		System.out.println("Passenger Capacity: " + travelPackage.getPassengerCapacity());
		System.out.println("Number of Passengers Enrolled: " + travelPackage.getPassengers().size());
		for (Passenger passenger : travelPackage.getPassengers()) {
			System.out.println("Passenger: " + passenger.getName() + ", Number: " + passenger.getPassengerNumber());
		}
	}

	@Override
	public void printIndividualPassengerDetails(Passenger passenger) {
		System.out.println(
				"Details for Passenger\n Passenger name: " + passenger.getName() + ", Number: " + passenger.getPassengerNumber());
		if (passenger instanceof StandardPassenger) {
			System.out.println("Passenger Type: Standard");
			System.out.println("Balance: $" + ((StandardPassenger) passenger).getBalance());
		} else if (passenger instanceof GoldPassenger) {
			System.out.println("Passenger Type: Gold");
			System.out.println("Balance: $" + ((GoldPassenger) passenger).getBalance());
		} else if (passenger instanceof PremiumPassenger) {
			System.out.println("Passenger Type: Premium");
			System.out.println("This passenger has free access to all activities.");
		}

		System.out.println("Activities Signed Up:");
		for (Activity activity : passenger.getSignedActivities()) {
//			System.out.println("Activity: " + activity.getName() + " at Destination: "
//					+ findDestinationByActivity(passenger, activity).getName());
			System.out.println("Activity: " + activity.getName() );
			if (passenger instanceof StandardPassenger) {
				System.out.println("Price Paid: $" + activity.getCost());
			} else if (passenger instanceof GoldPassenger) {
				double discountedPrice = activity.getCost() * 0.9; // Apply 10% discount for GoldPassenger
				System.out.println("Price Paid after 10% Discount: $" + discountedPrice);
			} else if (passenger instanceof PremiumPassenger) {
				System.out.println("This passenger has free access to all activities.");
			}
		}
	}

	@Override
	public void printAvailableActivities(TravelPackage travelPackage) {
		System.out.println("Available Activities:");
		for (Destination destination : travelPackage.getItinerary()) {
			for (Activity activity : destination.getActivities()) {
				if (activity.getCapacity() > 0) {
					System.out.println("Activity: " + activity.getName() + " at Destination: " + destination.getName());
					System.out.println("Spaces Available: " + activity.getCapacity());
				}
			}
		}
	}


	
}
