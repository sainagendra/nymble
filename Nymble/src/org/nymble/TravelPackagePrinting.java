package org.nymble;

public interface TravelPackagePrinting {
    void printItinerary(TravelPackage travelPackage);

    void printPassengerList(TravelPackage travelPackage);

    void printIndividualPassengerDetails(Passenger passenger);

    void printAvailableActivities(TravelPackage travelPackage);
}

