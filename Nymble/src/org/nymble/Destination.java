package org.nymble;

import java.util.ArrayList;
import java.util.List;

public class Destination {

	private String name;
    private List<Activity> activities;

    public Destination(String name) {
        this.name = name;
        this.activities = new ArrayList<>();
    }
    public Destination() {}
    public String getName() {
        return name;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void addActivity(Activity activity) {
        activities.add(activity);
    }

    // Other methods for managing activities in the destination
    public void removeActivity(Activity activity) {
        activities.remove(activity);
    }

    public boolean hasActivity(Activity activity) {
        return activities.contains(activity);
    }

    public boolean isActivityAvailable(Activity activity) {
        for (Activity a : activities) {
            if (a.getName().equals(activity.getName())) {
                return a.getCapacity() > 0;
            }
        }
        return false;
    }
   


   

    public void printActivities() {
        System.out.println("Activities at " + name + ":");
        for (Activity activity : activities) {
            System.out.println(activity.toString());
        }
    }
}
