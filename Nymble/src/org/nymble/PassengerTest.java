package org.nymble;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class PassengerTest {

	 @Test
	    public void testPassengerSignUpForActivity() {
		 Activity activity = new Activity("Hiking", "Exploring the mountains", 0, 5);
	        Passenger passenger = new Passenger("sai", 123);

	        passenger.signUpForActivity(activity);

	        List<Activity> signedActivities = passenger.getSignedActivities();
	        assertTrue(signedActivities.contains(activity)); // Check if activity is added to signed activities
	    }

	    @Test
	    public void testPassengerCancelActivity() {
	    	 Activity activity2 = new Activity("Snorkeling", "Underwater adventure", 80.0, 15);
	        Passenger passenger = new Passenger("loki", 456);

	        passenger.signUpForActivity(activity2);
	        assertTrue(passenger.cancelActivity(activity2)); // Check if the activity is successfully canceled

	        List<Activity> signedActivities = passenger.getSignedActivities();
	        assertFalse(signedActivities.contains(activity2)); // Ensure the activity is removed from signed activities
	    }

}
