package org.nymble;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ActivityTest {

	
	 @Test
	    public void testSignUp() {
	        Activity activity = new Activity("Hiking", "Exploring the mountains", 0, 5);

	        assertTrue(activity.canSignUp()); // Initially, there should be space to sign up

	        for (int i = 0; i < 5; i++) {
	            assertTrue(activity.signUp()); // Sign up 5 participants
	        }

	        assertFalse(activity.canSignUp()); // Activity is full now, can't sign up more
	        assertFalse(activity.signUp()); // Attempting to sign up should return false
	    }
}
