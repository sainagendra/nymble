package org.nymble;

public class Activity {
	
	 private String name;
	    private String description;
	    private double cost;
	    private int capacity;
	    private int currentParticipants;

	    public Activity(String name, String description, double cost, int capacity) {
	        this.name = name;
	        this.description = description;
	        this.cost = cost;
	        this.capacity = capacity;
	    }
         public Activity() {}
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public double getCost() {
			return cost;
		}

		public void setCost(double cost) {
			this.cost = cost;
		}

		public int getCapacity() {
			return capacity;
		}

		public void setCapacity(int capacity) {
			this.capacity = capacity;
		}
		
		
		 public int getCurrentParticipants() {
			return currentParticipants;
		}
		public void setCurrentParticipants(int currentParticipants) {
			this.currentParticipants = currentParticipants;
		}
		public boolean canSignUp() {
		        return currentParticipants < capacity;
		    }

		    public boolean signUp() {
		        if (canSignUp()) {
		            currentParticipants++;
		            return true;
		        } else {
		            System.out.println("Sorry, this activity is already full.");
		            return false;
		        }
		    }
		 @Override
		    public String toString() {
		        return "Activity{" +
		                "name='" + name + '\'' +
		                ", description='" + description + '\'' +
		                ", cost=" + cost +
		                ", capacity=" + capacity +
		                '}';
		    }
		 
}
