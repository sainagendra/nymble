package org.nymble;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class TravelPackageTest {

	 @Test
	    public void testAddDestination() {
	        TravelPackage travelPackage = new TravelPackage("Adventure Trip", 10);
	        Destination destination = new Destination("Mountain View");

	        travelPackage.addDestination(destination);

	        List<Destination> itinerary = travelPackage.getItinerary();
	        assertTrue(itinerary.contains(destination), "Destination not added to itinerary");
	    }

	    @Test
	    public void testAddPassenger() {
	        TravelPackage travelPackage = new TravelPackage("Adventure Trip", 2);
	        Passenger passenger1 = new Passenger("John", 101);
	        Passenger passenger2 = new Passenger("Alice", 102);

	        travelPackage.addPassenger(passenger1);
	        travelPackage.addPassenger(passenger2);

	        List<Passenger> passengers = travelPackage.getPassengers();
	        assertTrue(passengers.contains(passenger1), "Passenger 1 not added to package");
	        assertTrue(passengers.contains(passenger2), "Passenger 2 not added to package");
	    }

	    @Test
	    public void testAddPassengerBeyondCapacity() {
	        TravelPackage travelPackage = new TravelPackage("Adventure Trip", 1);
	        Passenger passenger1 = new Passenger("John", 101);
	        Passenger passenger2 = new Passenger("Alice", 102);

	        travelPackage.addPassenger(passenger1);
	        travelPackage.addPassenger(passenger2);

	        List<Passenger> passengers = travelPackage.getPassengers();
	        assertTrue(passengers.contains(passenger1), "Passenger 1 not added to package");
	        assertFalse(passengers.contains(passenger2), "Passenger 2 erroneously added beyond capacity");
	    }

}
