package org.nymble;

import java.util.ArrayList;
import java.util.List;

public class TravelPackage {
	private String name;
    private int passengerCapacity;
    private List<Destination> itinerary;
    private List<Passenger> passengers;
    
    public TravelPackage() {}

    public TravelPackage(String name, int passengerCapacity) {
        this.name = name;
        this.passengerCapacity = passengerCapacity;
        this.itinerary = new ArrayList<>();
        this.passengers = new ArrayList<>();
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPassengerCapacity() {
		return passengerCapacity;
	}

	public void setPassengerCapacity(int passengerCapacity) {
		this.passengerCapacity = passengerCapacity;
	}

	public List<Destination> getItinerary() {
		return itinerary;
	}

	public void setItinerary(List<Destination> itinerary) {
		this.itinerary = itinerary;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

	public void addDestination(Destination destination1) {
		itinerary.add(destination1);
		
	}

	public void addPassenger(Passenger passenger) {
		if (passengers.size() < passengerCapacity) {
			passengers.add(passenger);
		} else {
			System.out.println("Sorry, the package is full.");
		}
	}

	
    

}

